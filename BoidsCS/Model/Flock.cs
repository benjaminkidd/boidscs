﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BoidsCS;

namespace BoidsCS
{
    class Flock{

        public List<Boid> Boids = new List<Boid>();

        public Flock(int boundary){
            for (int i = 0; i < 15; i++){
                Boids.Add(new Boid((i > 12), boundary));
            }
        }

        public void MoveBoids() { 
            foreach(Boid boid in Boids)
            {
                boid.Move(Boids);
            }
        }
    }
}
