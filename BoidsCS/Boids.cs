﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace BoidsCS
{
    class Boids: Form{
        private Timer timer;
        private Flock flock;
        private Image iconRegular;
        private Image iconZomble;


        [STAThread]
        private static void Main(){
            Application.Run(new Boids());

        }

        public Boids(){

            int boundary = 1080; //screen size
            //setting up the viewing area
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true); //???
            FormBorderStyle = FormBorderStyle.FixedToolWindow;
            StartPosition = FormStartPosition.CenterScreen;
            ClientSize = new Size(boundary, boundary);

            //the two different types of flockers
            iconRegular = CreateIcon(Brushes.Blue);
            iconZomble = CreateIcon(Brushes.Red);

            flock = new Flock(boundary);

            timer = new Timer();
            timer.Tick += new EventHandler(this.timer_Tick); // lol timers
            timer.Interval = 75;
            timer.Start();

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            foreach (Boid boid in flock.Boids) {

                float angle;
                if (boid.dX == 0) angle = 90f; // if its x is 0 then do a 90degree turn
                else angle = (float)(Math.Atan(boid.dY / boid.dX) * 57.3); //MATHS EWW
                if (boid.dX < 0f) angle += 180f; // turn around if its X is off screen e.g. less than 0

                Matrix matrix = new Matrix();
                matrix.RotateAt(angle, boid.Position);
                e.Graphics.Transform = matrix;
                if (boid.Zomble) e.Graphics.DrawImage(iconZomble, boid.Position); // if its a zomble draw a zomble at its position
                else e.Graphics.DrawImage(iconRegular, boid.Position);
            }

        }
        // create the icon for the boid (triangle
        private static Image CreateIcon(Brush brush){
            Bitmap icon = new Bitmap(16, 16);
            Graphics g = Graphics.FromImage(icon);
            //drawing a triangle
            Point p1 = new Point(0, 16);
            Point p2 = new Point(16, 8);
            Point p3 = new Point(0, 0);
            Point p4 = new Point(5, 8);
            Point[] points = { p1, p2, p3, p4 };
            g.FillPolygon(brush, points);
            return icon;

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            flock.MoveBoids();
            Invalidate();
        }

    }
}
